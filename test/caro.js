const chai = require('chai');
const expect = chai.expect;

describe('Các trường hợp thắng', () => {
  it('1 bên có 4 trắng sẽ thắng', () => {
    expect(1).to.equal(1);
  });
  it('1 bên chạm 5 sẽ thắng', () => {
    expect(1).to.equal(1);
  });
  it('1 bên có 3 trắng nước đôi sẽ thắng', () => {
    expect(1).to.equal(1);
  });
  it('1 bên có 4 chạm 1 đâu + 3 trắng sẽ thắng', () => {
    expect(1).to.equal(1);
  });
  it('1 bên có 2 4 chặn 1 đầu sẽ thắng', () => {
    expect(1).to.equal(1);
  });
});

